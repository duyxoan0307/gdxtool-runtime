package editor.sceneloader;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.ObjectMap;
import editor.actor.GActor;
import editor.object.ActorUtils;

public class MapActor {
    public ObjectMap<String,Actor> map = new ObjectMap<>();
    public MapActor(Actor actor){
        ActorUtils.traverseActor(actor,(a -> {
            map.put(ActorUtils.getActorName(a),a);
        }));
    }
    public <T extends Actor>T find(String key){
        return (T)map.get(key);
    }

    public <T extends Actor>T btn(String name,Runnable cb){
        return GActor.get(find(name)).effBtn().addListener(cb).get();
    }

    public <T extends Actor>T btn(Actor actor, Runnable cb){
        return GActor.get(actor).effBtn().addListener(cb).get();
    }
}

package editor.sceneloader;

import com.badlogic.gdx.utils.*;

public class SceneUtils {

    public static void getAssetToLoad(String json, ITraverseAsset cb){
        JsonValue jsonValue = new JsonReader().parse(json);
        traverseJson(jsonValue,cb);
    }
    public interface ITraverseAsset{
        void onTraverseAsset(String fieldName, String data);
    }
    private static void traverseJson(JsonValue jsonValue, ITraverseAsset cb) {
        if (jsonValue.isObject()) {
            // If it's an object, iterate through its fields
            for (JsonValue child : jsonValue) {
                traverseJson(child,cb);
            }
        } else if (jsonValue.isArray()) {
            // If it's an array, iterate through its elements
            for (JsonValue child : jsonValue) {
                // Recursively traverse through the child
                traverseJson(child,cb);
            }
        } else {
            String fieldName = jsonValue.name;
            if (isAsset(fieldName)){
                String val = jsonValue.asString();
                if (val!=null && !val.equals("")){
                    cb.onTraverseAsset(fieldName,val);
                }
            }
        }
    }
    public static boolean isAsset(String fieldName){
        if (isTexture(fieldName))
            return true;
        if (isFont(fieldName))
            return true;
        if (isParticle(fieldName))
            return true;
        if (isSpine(fieldName))
            return true;
        if (isAtlas(fieldName))
            return true;
        return false;
    }
    public static boolean isTexture(String fieldName){
        return fieldName.equals("texture");
    }
    public static boolean isFont(String fieldName){
        return fieldName.equals("font");
    }
    public static boolean isParticle(String fieldName){
        return fieldName.equals("particleName");
    }
    public static boolean isSpine(String fieldName){
        return fieldName.equals("spine");
    }
    public static boolean isAtlas(String fieldName){
        return fieldName.equals("atlas");
    }
}

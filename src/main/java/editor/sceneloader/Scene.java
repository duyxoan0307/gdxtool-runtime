package editor.sceneloader;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import editor.actor.ReferenceObject;
import editor.object.ActorParser;
import editor.object.ActorUtils;
import editor.object.GameObject;
import editor.object.GameObjectUtils;

public class Scene {
    public static Scene curScene;
    public static Scene loadScene(String data){
        Scene scene = new Scene(data);
        curScene=scene;
        return scene;
    }
    public Group root;
    public IntMap<GameObject> mapUUID;
    public Scene(String data){
        ReferenceObject.startTrack();
        root = (Group) ActorParser.jsonToActor(data);
        ReferenceObject.stopTrack();

        mapUUID=getMapUUID(root);
        setReferences();
        ActorUtils.traverseActor(root, actor -> {
            instantiate(actor);
        });
    }
    public void setReferences(){
        setReferences(mapUUID);
    }
    public static void setReferences(IntMap<GameObject> mapUUID){
        for (ReferenceObject referenceObject:ReferenceObject.instances){
            referenceObject.setGameObjectAndCast(mapUUID.get(referenceObject.getId()));
        }
    }
    public static void setReferences(Actor root){
        setReferences(getMapUUID(root));
    }
    public static void setReferecesClone(Actor clonedActor){
        for (ReferenceObject referenceObject:ReferenceObject.instances){
            GameObject gameObject = referenceObject.getGameObject();
            if (gameObject!=null){
                Actor child = findActor(clonedActor,ActorUtils.getActorName(gameObject.actor));
                GameObject goFromChild = GameObjectUtils.getGameObjectFromActor(child);
                if (goFromChild!=null){
                    referenceObject.setId(goFromChild.uuid);
                    referenceObject.setGameObjectAndCast(goFromChild);
                }
            }

        }
    }

    public static Actor findActor(Actor actor, String name){
        if (actor.getName()!=null && actor.getName().equals(name)){
            return actor;
        }
        if (actor instanceof Group){
            return ((Group)actor).findActor(name);
        }
        return null;
    }

    public static IntMap<GameObject> getMapUUID(Actor actor){
        IntMap<GameObject> mapUUID = new IntMap();
        ActorUtils.traverseActor(actor, ac -> {
            GameObject go = GameObjectUtils.getGameObjectFromActor(ac);
            if (go!=null){
                mapUUID.put(go.uuid,go);
            }
        });
        return mapUUID;
    }
    public Actor clone(String data){
        ReferenceObject.startTrack();
        Actor actor = ActorParser.jsonToActor(data);
        ReferenceObject.stopTrack();
        setReferences();
        setReferecesClone(actor);
        ActorUtils.traverseActor(actor, _actor -> {
            instantiate(_actor);
        });
        return actor;
    }
    public Actor clone(Actor actor){
        return clone(ActorParser.actorToJson(actor));
    }


    public static Actor instantiate(Actor actor){
        GameObject go = GameObjectUtils.getGameObjectFromActor(actor);
        if (go!=null){
            go.start();
        }
        return actor;
    }
}

package editor.sceneloader;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.ObjectMap;
import editor.actor.ReferenceObject;
import editor.object.ActorUtils;
import editor.util.FileUtil;
import editor.object.ActorParser;

import java.sql.Ref;

public class SceneLoader {
    private static SceneLoader instance;
    public ObjectMap<String, Scene> mapScenes;

    public static SceneLoader get() {
        if (instance == null) {
            instance = new SceneLoader();
//            WorldController.get().clearBodies();
        }
        return instance;
    }

    String defaultScene="";
    public void load(String path) {
        mapScenes = new ObjectMap<>();
        FileHandle sceneFolder = FileUtil.getFile(path);
        FileHandle[] childs = FileUtil.getListFile(sceneFolder);
        for (FileHandle child : childs) {
            loadScene(child);
        }
        if (childs.length>0){
            setDefaultScene(childs[0].nameWithoutExtension());
        }
    }
    public void setDefaultScene(String s){
        defaultScene = s;
    }

    private void loadScene(FileHandle file) {
        mapScenes.put(file.nameWithoutExtension(), new Scene(FileUtil.readString(file)));
    }

    public Actor get(String sceneName, String actorName) {
//        Actor res = ActorParser.cloneActorWithRef(mapScenes.get(sceneName).findActor(actorName));
        Actor res = mapScenes.get(sceneName).root.findActor(actorName);
        res.setVisible(true);
        return res;
    }

    public <T extends Actor> T get(String actorName) {
        return (T)get(defaultScene,actorName);
    }
}

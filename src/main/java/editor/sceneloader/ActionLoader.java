package editor.sceneloader;

import com.badlogic.gdx.scenes.scene2d.Action;
import editor.action.ActionData;
import editor.action.ActionParser;
import editor.util.FileUtil;

public class ActionLoader {
    public static ActionData fileToData(String fileName){
        return ActionParser.jsonToData(FileUtil.readString("action/"+fileName));
    }
    public static Action fileToAction(String fileName){
        return ActionParser.dataToAction(fileToData(fileName));
    }
}

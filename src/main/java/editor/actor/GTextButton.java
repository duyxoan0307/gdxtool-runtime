package editor.actor;

import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;

import editor.util.GUI;

public class GTextButton extends GGroup {
    public Image bg_img;
    public Label title_label;
    public GTextButton(Image bg, Label title){
        this.bg_img=bg;
        addActor(bg_img);
        this.bg_img.setPosition(0,0, Align.center);

        this.title_label=title;
        addActor(title_label);
        title_label.setSize(bg_img.getWidth()-70,bg.getHeight());
        GUI.fitLabel(title_label,1);
        title_label.setPosition(0,0,Align.center);

        this.title_label.setTouchable(Touchable.disabled);

        this.setSize(bg_img.getWidth(),bg.getHeight());
        GActor.get(bg_img).centerAt(this);
        GActor.get(title_label).centerAt(this).fitLabel(1);
    }
}

package editor.actor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import editor.provider.ToolProvider;
import editor.util.GUI;

public class GLabel extends Label {
    public static final String FIT_KEY = "fit";
    public GLabel(){
        super("",new LabelStyle(ToolProvider.get().getFont("font_white"), Color.WHITE));

    }
    public GLabel(CharSequence text, LabelStyle style) {
        super(text, style);
    }
    public void setText(CharSequence newText) {
        super.setText(newText);
        Float fit = (Float) GActor.getUserObject(this,FIT_KEY);
        if (fit!=null){
            GUI.fitLabel(this, fit);
        }
    }

    @Override
    public void setFontScale(float fontScale) {
        if (fontScale<=0){
            fontScale=0.1f;
        }
        super.setFontScale(fontScale);
    }
}

package editor.actor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import com.badlogic.gdx.utils.ObjectMap;
import editor.object.GAlign;
import editor.object.GameObjectUtils;
import editor.provider.ToolProvider;
import editor.util.GUI;
public class GActor {
    public static GActor gActor = new GActor();
    Actor actor;
    public GActor setActor(Actor actor){
        this.actor=actor;
        return this;
    }
    public GActor setDefault(){
        this.actor.setPosition(0,0, Align.center);
        this.actor.setOrigin(Align.center);
        return this;
    }
    public GActor x(float x){
        this.actor.setX(x,Align.center);
        return this;
    }
    public GActor x(float x, int align){
        this.actor.setX(x,align);
        return this;
    }
    public GActor y(float y){
        this.actor.setY(y,Align.center);
        return this;
    }
    public GActor y(float y, int align){
        this.actor.setY(y,align);
        return this;
    }
    public GActor pos(float x, float y){
        this.actor.setPosition(x,y,Align.center);
        return this;
    }
    public GActor pos(Vector2 pos){
        return pos(pos.x,pos.y);
    }
    public GActor posByParent(float x, float y){
        return posByParent(x,y,Align.center);
    }
    public GActor posByParent(float x, float y, int align){
        this.actor.setPosition(actor.getParent().getWidth()/2+x,actor.getParent().getHeight()/2+y,align);
        return this;
    }
    public GActor pos(Actor actor){
        return this.pos(actor.getX(Align.center),actor.getY(Align.center));
    }
    public GActor pos(Actor actor, int alignRelative, int alignMe){
        this.actor.setPosition(actor.getX(alignRelative),actor.getY(alignRelative),alignMe);
        return this;
    }
    public Vector2 vecPos(){
        return new Vector2(actor.getX(Align.center),actor.getY(Align.center));
    }
    public GActor pos(float x, float y, int align){
        this.actor.setPosition(x,y, align);
        return this;
    }
    static Actor pivotActor = createPivotActor();
    static Actor createPivotActor(){
        return GActor.get(new Actor()).stretch().get();
    }
    public GActor pivot(int align){
        if (Align.isTop(align)){
            float dY = ToolProvider.get().getH() /2-actor.getY(Align.top);
            actor.setY(ToolProvider.get().halfStageH()-dY,Align.top);
        }else if (Align.isBottom(align)){
            float dY = -ToolProvider.get().getH()/2-actor.getY(Align.bottom);
            actor.setY(-ToolProvider.get().halfStageH()-dY,Align.bottom);
        }
        if (Align.isLeft(align)){
            float dX = -ToolProvider.get().getW()/2-actor.getX(Align.left);
            actor.setX(-ToolProvider.get().halfStageW()-dX,Align.left);
        }else if (Align.isRight(align)){
            float dX = ToolProvider.get().getW()/2-actor.getX(Align.right);
            actor.setX(ToolProvider.get().halfStageW()-dX,Align.right);
        }
        return this;
    }
    public GActor pivotParent(int align){
        Actor parent = actor.getParent();
        if (Align.isTop(align)){
            float dY = parent.getHeight()-actor.getY(Align.top);
            actor.setY(parent.getHeight()-dY,Align.top);
        }else if (Align.isBottom(align)){
            float dY = -actor.getY(Align.bottom);
            actor.setY(-dY,Align.bottom);
        }
        if (Align.isLeft(align)){
            float dX =-actor.getX(Align.left);
            actor.setX(-dX,Align.left);
        }else if (Align.isRight(align)){
            float dX =parent.getWidth()-actor.getX(Align.right);
            actor.setX(parent.getWidth()-dX,Align.right);
        }
        return this;
    }
    public static Vector2 getPivot(Vector2 pos,int align){
        Vector2 res = new Vector2(pos);
        if (Align.isTop(align)){
            float dY = ToolProvider.get().getH()/2-res.y;
            res.y=(ToolProvider.get().halfStageH()-dY);
        }else if (Align.isBottom(align)){
            float dY = -ToolProvider.get().getH()/2-res.y;
            res.y=(-ToolProvider.get().halfStageH()-dY);
        }
        if (Align.isLeft(align)){
            float dX = -ToolProvider.get().getW()/2-res.x;
            res.x=(-ToolProvider.get().halfStageW()-dX);
        }else if (Align.isRight(align)){
            float dX = ToolProvider.get().getW()/2-res.x;
            res.x=(ToolProvider.get().halfStageW()-dX);
        }
        return res;
    }

    public GActor posByScreen(int alignScene, int alignMe){
        return pos(pivotActor,alignScene,alignMe);
    }
    public GActor moveBy(float x, float y){
        this.actor.moveBy(x,y);
        return this;
    }
    public GActor moveX(float x){
        this.actor.moveBy(x,0);
        return this;
    }
    public GActor moveY(float y){
        this.actor.moveBy(0,y);
        return this;
    }
    public GActor follow(Actor follow, float dX, float dY){
        Actor me = this.actor;
        me.addAction(new Action() {
            @Override
            public boolean act(float delta) {
                me.setPosition(follow.getX(Align.center)+dX,follow.getY(Align.center)+dY);
                return false;
            }
        });
        return this;
    }
    public GActor initSize(){
        if (actor instanceof Image){
            TextureRegionDrawable regionDrawable = (TextureRegionDrawable) ((Image) actor).getDrawable();
            TextureRegion region = regionDrawable.getRegion();
            size(region.getRegionWidth(),region.getRegionHeight());
        }
        return this;
    }
    public GActor size(float w, float h){
        Vector2 pos = GUI.actorStagePos(actor);
//        boolean is_origin_center = isActorOriginCenter(actor);

        GAlign curAlignVal = GAlign.getAlign(actor);
        this.actor.setSize(w,h);
//        if (is_origin_center)
//            origin(Align.center);
        if (curAlignVal!=null){
            origin(curAlignVal.align);
        }
        pos(pos);
        return this;
    }
    public GActor sizeByChild(){
        if (actor instanceof Group){
            Group group = (Group)actor;
            if (group.getChildren().size>0){
                Actor child = group.getChild(0);
                Vector2 p = new Vector2(group.getX(Align.center),group.getY(Align.center));
                group.setSize(child.getWidth(),child.getHeight());
                group.setPosition(p.x,p.y,Align.center);
                float dtX = child.getX();
                float dtY = child.getY();
                for (Actor children:group.getChildren()){
                    children.moveBy(-dtX,-dtY);
                }
                child.setPosition(0,0,Align.bottomLeft);
            }
        }
        return this;
    }
    public static boolean isActorOriginCenter(Actor actor){
        return actor.getOriginX()==actor.getWidth()/2 && actor.getOriginY()==actor.getHeight()/2;
    }

    public GActor rotate(float rotation){
        this.actor.setRotation(rotation);
        return this;
    }
    public GActor rotateBy(float rotation){
        this.actor.rotateBy(rotation);
        return this;
    }
    public GActor visible(boolean visible){
        this.actor.setVisible(visible);
        return this;
    }
    public GActor touchable(Touchable touchable){
        this.actor.setTouchable(touchable);
        return this;
    }
    public GActor touchable(boolean touchable){
        this.actor.setTouchable(touchable? Touchable.enabled:Touchable.disabled);
        return this;
    }
    public GActor name(String name){
        this.actor.setName(name);
        return this;
    }
    public GActor stretch(){
        return stretchW().stretchH().pos(0,0);
    }
    public GActor stretchW(){
        Vector2 pos = new Vector2(this.actor.getX(Align.center),this.actor.getY(Align.center));
        this.actor.setWidth(ToolProvider.get().halfStageW()*2);
        pos(0,pos.y);
        return this;
    }
    public GActor stretchH(){
        Vector2 pos = new Vector2(this.actor.getX(Align.center),this.actor.getY(Align.center));
        this.actor.setHeight(ToolProvider.get().halfStageH()*2);
        pos(pos.x,0);
        return this;
    }
    public GActor texture(String name){
        return texture(ToolProvider.get().getTextureRegion(name));
    }
    public GActor texture(TextureRegion textureRegion){
        if (actor instanceof Image){
            GUI.setRegion((Image)actor,textureRegion);
        }
        return this;
    }
    public GActor scl(float scl){
        this.actor.setScale(scl);
        return this;
    }
    public GActor scl(float x, float y){
        this.actor.setScale(x,y);
        return this;
    }
    public GActor sclSize(float scl){
        this.actor.setSize(this.actor.getWidth()*scl,this.actor.getHeight()*scl);
        return this;
    }
    public GActor sclX(float x){
        this.actor.setScaleX(x);
        return this;
    }
    public GActor sclY(float y){
        this.actor.setScaleY(y);
        return this;
    }
    public GActor origin(int align){
        this.actor.setOrigin(align);
        return this;
    }
    public GActor origin(float x, float y){
        this.actor.setOrigin(x,y);
        return this;
    }
    public GActor color(Color color){
        actor.setColor(color);
        return this;
    }
    public GActor opacity(float a){
        actor.getColor().a=a;
        return this;
    }
    public GActor fontScl(float scale){
        if (actor instanceof Label){
            ((Label) actor).setFontScale(scale);
//            actor.setScale(scale);
        }
        return this;
    }
    public GActor font(BitmapFont font){
        if (actor instanceof Label){
            Label.LabelStyle style = new Label.LabelStyle();
            style.font = font;
            ((Label) actor).setStyle(style);
        }
        return this;
    }
    public GActor centerAt(Actor actor){
        this.actor.setPosition(actor.getX(Align.center),actor.getY(Align.center),Align.center);
        return this;
    }
    public GActor run(IActorRunable runable){
        Actor actor = this.actor;
        runable.run(actor);
        return this;
    }
    public GActor drawable(String s) {
        Actor actor = this.actor;
        if (actor instanceof Image){
            ((Image) actor).setDrawable(new TextureRegionDrawable(ToolProvider.get().getTextureRegion(s)));
            setUserObject(actor, GameObjectUtils.TEXTURE_KEY,s);
        }
        return this;
    }
    public GActor drawableResize(String s) {
        if (actor instanceof Image){
            GUI.setRegion((Image) actor, ToolProvider.get().getTextureRegion(s),true);
        }
        return this;
    }

    public interface IActorRunable<T extends Actor>{
        void run(T actor);
    }
    public GActor btnTextureState(String up, String down){
        Actor _actor = this.actor;
        TextureRegion upRegion = ToolProvider.get().getTextureRegion(up);
        TextureRegion downRegion = ToolProvider.get().getTextureRegion(down);
        _actor.addListener(new GClickListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                Image img = (Image) _actor;
                GUI.setRegion(img, upRegion,true,Align.center);
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                Image img = (Image) _actor;
                GUI.setRegion(img, downRegion,true,Align.center);
                return super.touchDown(event, x, y, pointer, button);
            }
        });
        return this;
    }
    public GActor effBtn(){
        Actor _actor = this.actor;
        _actor.addListener(new GClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
            }

            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                super.enter(event, x, y, pointer, fromActor);
                _actor.getColor().a=0.7f;
            }

            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                super.exit(event, x, y, pointer, toActor);
                _actor.getColor().a=1;
            }
        });
        return this;
    }

    public GActor effBtnScale(){
        Actor _actor = this.actor;
        _actor.addListener(new GClickListener() {
            float cur_scale_x = _actor.getScaleX();
            float cur_scale_y = _actor.getScaleY();
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                ToolProvider.get().playSound("button",0.5f);
            }

            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                super.enter(event, x, y, pointer, fromActor);
                _actor.addAction(Actions.scaleTo(cur_scale_x*1.3f,cur_scale_y*1.3f,0.2f));
            }

            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                super.exit(event, x, y, pointer, toActor);
                _actor.addAction(Actions.scaleTo(cur_scale_x,cur_scale_y,0.2f));
                _actor.getColor().a=1;
            }
        });
        return this;
    }
    public GActor addListener(Runnable cb){
        actor.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (cb!=null)
                    cb.run();
            }
        });
        return this;
    }
    public GActor addListener(ClickListener listener){
        actor.addListener(listener);
        return this;
    }
    public GActor parent(Group parent){
        if (parent!=null)
            parent.addActor(actor);
        return this;
    }
    public GActor getChild(int i){
        return setActor(((Group)actor).getChild(i));
    }
    public GActor child(Actor child){
        if (actor instanceof Group)
            ((Group)actor).addActor(child);
        return this;
    }
    public GActor transform(boolean transform){
        if (this.actor instanceof Group){
            ((Group) this.actor).setTransform(transform);
        }
        return this;
    }
    public GActor alignLabel(int align){
        if (actor instanceof Label){
            ((Label) actor).setAlignment(align);
        }
        else if (actor instanceof TextField){
            ((TextField) actor).setAlignment(align);
        }
        else if (actor instanceof TextArea){
            ((TextArea) actor).setAlignment(align);
        }
        return this;
    }
    public GActor fitLabel(float fontScale){
        if (actor instanceof Label){
            GActor.setUserObject(actor,GLabel.FIT_KEY,fontScale);
            ((Label) actor).setText(((Label) actor).getText());
        }
        return this;
    }
    public GActor wrap(boolean is){
        if (actor instanceof Label){
            ((Label) actor).setWrap(is);
        }
        return this;
    }
    public GActor text(String text){
        if (actor instanceof Label){
            ((Label) actor).setText(text);
        }
        else if (actor instanceof TextField){
            ((TextField) actor).setText(text);
        }
        else if (actor instanceof TextArea){
            ((TextArea) actor).setText(text);
        }
        return this;
    }

    public GActor zIndex(int id){
        actor.setZIndex(id);
        return this;
    }
    public GActor zIndexChange(int id){
        int curId = actor.getZIndex();
        actor.setZIndex(curId+id);
        return this;
    }
    public GActor debug(boolean debug){
        if (this.actor instanceof Group){
            ((Group)actor).setDebug(debug,true);
            return this;
        }
        this.actor.setDebug(debug);
        return this;
    }
    public GActor debugAll(){
        if (this.actor instanceof Group){
            ((Group) this.actor).debugAll();
        }
        return this;
    }
    public static GActor textField(String cursor_texture, String bg_texture, String selection_texture, String font_name){
        TextField textArea = new GTextField("",GUI.getTextFieldStyle(cursor_texture, bg_texture, selection_texture, font_name));
        return GActor.newActor(textArea);
    }
    public static GActor textArea(String cursor_texture, String bg_texture, String selection_texture, String font_name){
        TextArea textArea = new TextArea("",GUI.getTextFieldStyle(cursor_texture, bg_texture, selection_texture, font_name));
        return GActor.newActor(textArea);
    }

    public GActor action(Action action){
        this.actor.addAction(action);
        return this;
    }
    public GActor clearListeners(){
        this.actor.clearListeners();
        return this;
    }
    public GActor clearAction(){
        this.actor.clearActions();
        return this;
    }
    public <T>T get(){
        return (T)actor;
    }
    public <T>T get(Class<T> classOfT){
        return (T)actor;
    }


    //factory
    public static GActor img(TextureRegion region){
        Image img;
        img = new Image();
        img.setDrawable(new TextureRegionDrawable(region));
        img.setSize(region.getRegionWidth(),region.getRegionHeight());
        return GActor.newActor(img);
    }
    public static GActor img(Texture texture){
        return img(new TextureRegion(texture));
    }
    public static GActor img(String texture){
        return img(ToolProvider.get().getTextureRegion(texture));
    }
    public static GActor btn(TextureRegion region){
        return img(region).effBtn();
    }
    public static GActor btn(Texture texture){
        return img(texture).effBtn();
    }
    public static GActor btn(String texture){
        return img(texture).effBtn();
    }
    public static GActor label(String text, String font_name){
        return label(text, ToolProvider.get().getFont(font_name));
    }
    public static GActor label(String text){
        return label(text,"font_white");
    }
    public static GActor label(String text, BitmapFont font){
        Label label;
        label=new GLabel(text,new Label.LabelStyle(font,Color.WHITE));
        label.setText(text);
        label.setStyle(new Label.LabelStyle(font,Color.WHITE));
        label.setAlignment(Align.center);
        label.setSize(label.getPrefWidth(),label.getPrefHeight());

        return GActor.newActor(label).text(text);
    }
    public static GActor textBtn(String region, String text){
        return textBtn(region,text,"font_white");
    }
    public static GActor textBtn(TextureRegion region, String text, BitmapFont font){
        GTextButton gTextButton = new GTextButton((Image)img(region).get(),(Label)label(text, font).get());
        return GActor.newActor(gTextButton).effBtn();
    }
    public static GActor textBtn(String region, String text, String font){
        return textBtn(ToolProvider.get().getTextureRegion(region),text,ToolProvider.get().getFont(font));
    }
    public static GActor group(){
        return GActor.newActor(new GGroup());
    }
    public static GActor scroll(){
        return GActor.newActor(new ScrollPane(null));
    }
    public static GActor scroll(Table table){
        return GActor.newActor(new ScrollPane(table));
    }
    public static GActor table(){
        return GActor.newActor(new Table().top());
    }
//    public static GActor particle(ParticleEffect pe){
//        GParticle gParticle = new GParticle(pe);
//        return GActor.newActor(gParticle);
//    }
//    public static GActor particle(String name){
//        return GActor.particle(LoaderImp.getParticle(name));
//    }
    public static GActor particle(String name){
        GParticle gParticle = new GParticle(name);
        return GActor.newActor(gParticle);
    }
    public static GActor newActor(Actor actor){
        return gActor.setActor(actor).setDefault();
    }
    public static GActor get(Actor actor){
        return gActor.setActor(actor);
    }

    public static void setUserObject(Actor actor, String key, Object obj){
        Object object = actor.getUserObject();
        ObjectMap map;
        if (object!=null && object instanceof ObjectMap){
            map = (ObjectMap)object;
        }else{
            map = new ObjectMap<String,Object>();
            actor.setUserObject(map);
        }
        map.put(key,obj);
    }
    public static Object getUserObject(Actor actor, String key){
        Object object = actor.getUserObject();
        if (object!=null && object instanceof ObjectMap){
            ObjectMap map = (ObjectMap)object;
            return map.get(key);
        }
        return null;
    }
    public static void removeUserObject(Actor actor, String key){
        Object object = actor.getUserObject();
        if (object!=null && object instanceof ObjectMap){
            ((ObjectMap)object).remove(key);
        }
    }


    public static GActor btnTextureState(String name){
        return btn(name+" normal").btnTextureState(name+" normal",name+" push");
    }
}

package editor.actor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.scenes.scene2d.ui.ParticleEffectActor;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.Field;
import editor.provider.ToolProvider;

public class GParticle extends ParticleEffectActor{
    public boolean loop;
    public String particleName;
    public GParticle(){
        super(new ParticleEffect(),false);
    }
    public GParticle(String particleName){
        super(ToolProvider.get().getEffect(particleName),true);
        this.particleName = particleName;
        start();
    }
    static ParticleEffect getEffect(String name){
        try {
            return ToolProvider.get().getEffect(name);
        }catch (Throwable e){
            return new ParticleEffect();
        }
    }
    public GParticle change(String name){
        try {
            Field field = ClassReflection.getDeclaredField(ParticleEffectActor.class,"particleEffect");
            field.setAccessible(true);
            ParticleEffect effect = getEffect(name);
            field.set(this,effect);
            this.particleName = name;
            this.isRunning=true;
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return this;
    }
    public GParticle loop(boolean loop){
        this.loop = loop;
        return this;
    }
    public GParticle scale(float scale){
        getEffect().scaleEffect(scale);
        return this;
    }

    @Override
    public void act(float delta) {
        if (getEffect().isComplete()){
            if (loop){
                start();
            }
        }
        super.act(delta);
    }

    public GParticle setColorParticle(int from, int to, Color color){
        for (int i=from;i<=to;i++){
            ParticleEmitter emitter = getEffect().getEmitters().get(i);
            float temp[] = emitter.getTint().getColors();
            temp[0] = color.r;
            temp[1] = color.g;
            temp[2] = color.b;
        }
        return this;
    }

    @Override
    public boolean remove() {
//        pe.dispose();
        if (getEffect() instanceof ParticleEffectPool.PooledEffect){
            ((ParticleEffectPool.PooledEffect) getEffect()).free();
        }else{
            getEffect().dispose();
        }
        return super.remove();
    }
}

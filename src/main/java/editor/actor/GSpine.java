package editor.actor;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pool;
import com.esotericsoftware.spine.*;
import com.esotericsoftware.spine.utils.SkeletonActor;
import com.esotericsoftware.spine.utils.SkeletonActorPool;
import editor.provider.ToolProvider;

public class GSpine {
    public static SkeletonRenderer skeletonRenderer = new SkeletonRenderer();
    static {
        skeletonRenderer.setPremultipliedAlpha(true);
    }
    public static ObjectMap<SkeletonData, SkeletonActorPool> poolMap = new ObjectMap<>();
    public static SkeletonActorPool getPool(SkeletonData data){
        if (!poolMap.containsKey(data)){
            int size = 20;
            SkeletonActorPool pool = new SkeletonActorPool(skeletonRenderer,data,new AnimationStateData(data),size,size){
                @Override
                protected SkeletonActor newObject() {
                    PooledSkeletonActor pooledSkeletonActor = new PooledSkeletonActor();
                    pooledSkeletonActor.setRenderer(skeletonRenderer);
                    return pooledSkeletonActor;
                }
            };
            pool.fill(size);
            poolMap.put(data,pool);
        }
        return poolMap.get(data);
    }

    public GSpine defSkin() {
        Array<Skin> skins = skeletonActor.getSkeleton().getData().getSkins();
        if (skins.size>0)
            setSkin(skeletonActor.getSkeleton().getData().getSkins().first().getName());
        return this;
    }

    public static class PooledSkeletonActor extends SkeletonActor implements Pool.Poolable {
        @Override
        public boolean remove() {
            return super.remove();
        }

        boolean outOfScreen(){
            return Math.abs(getX())>400 || Math.abs(getY())>700;
        }

        @Override
        public void reset() {
        }
    }
    public static PooledSkeletonActor getSkelActor(SkeletonData data){
        PooledSkeletonActor skeletonActor = new PooledSkeletonActor();
        skeletonActor.setRenderer(skeletonRenderer);
        try {
            skeletonActor.setSkeleton(new Skeleton(data));
            skeletonActor.setAnimationState(new AnimationState(new AnimationStateData(data)));
        }catch (Throwable t){
            t.printStackTrace();
        }
        return skeletonActor;
    }
    public static SkeletonActor getSkelActor(String name){
        SkeletonData skeletonData = ToolProvider.get().getSkeletonData(name);
        SkeletonActor skeletonActor = getSkelActor(skeletonData);
//        skeletonActor.setResetBlendFunction(false);
        skeletonActor.getAnimationState().getData().setDefaultMix(.1f);
        return skeletonActor;
    }

    private SkeletonActor skeletonActor;
    public GSpine (SkeletonActor skeletonActor){
        this.skeletonActor = skeletonActor;
    }
    public GSpine(String name){
        try {
            this.skeletonActor = getSkelActor(name);
        }catch (Throwable t){
            this.skeletonActor = new PooledSkeletonActor();
        }
    }
    public SkeletonActor getSkelActor(){
        return skeletonActor;
    }
    public GSpine setAnim(int id, String name, boolean loop){
        skeletonActor.getAnimationState().setAnimation(id,name,loop);
        return this;
    }

    public GSpine addAnim(int id, String name, boolean loop, float delay){
        skeletonActor.getAnimationState().addAnimation(id,name,loop,delay);
        return this;
    }
    public GSpine setSkin(String skin){
        skeletonActor.getSkeleton().setSkin(skin);
        return this;
    }
    public GSpine scl(float scl){
        skeletonActor.getSkeleton().setScale(scl,scl);
        return this;
    }
    public GSpine pos(float x,float y){
        skeletonActor.setPosition(x,y);
        return this;
    }
    public GSpine parent(Group group){
        group.addActor(getSkelActor());
        return this;
    }
    public GSpine setDefaultMix(float mix){
        skeletonActor.getAnimationState().getData().setDefaultMix(mix);
        return this;
    }
    public GSpine run(GActor.IActorRunable runable){
        runable.run(skeletonActor);
        return this;
    }
    public GSpine insertAnim(int id, String name){
        Animation curAnim = skeletonActor.getAnimationState().getCurrent(id).getAnimation();
        setAnim(id,name,false).addAnim(id,curAnim.getName(),true,0);
        return this;
    }

    public GSpine defAnim() {
        String anim = skeletonActor.getSkeleton().getData().getAnimations().first().getName();
        setAnim(0,anim,true);
        return this;
    }
}

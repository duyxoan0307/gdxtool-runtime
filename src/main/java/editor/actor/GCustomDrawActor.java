package editor.actor;

import com.badlogic.gdx.graphics.g2d.Batch;
import editor.object.cb.Run;

public class GCustomDrawActor extends GGroup {
    public Run.ICallback2<Batch,Float> cbDraw;
    public Run.ICallback<Float> cbAct;
    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (cbDraw!=null){
            cbDraw.run(batch,parentAlpha);
        }
        super.draw(batch, parentAlpha);
    }

    @Override
    public void act(float var1) {
        super.act(var1);
        if (cbAct!=null){
            cbAct.run(var1);
        }
    }
}

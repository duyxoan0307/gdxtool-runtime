package editor.actor;


import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Align;

public class GGroup extends Group {
    public GGroup(){
        setTransform(false);
    }
    private boolean pause;

    public void act(float var1) {
        if (!this.isVisible()){
            return;
        }
        if(!this.pause) {
            super.act(var1);
        }
    }
    void checkTransform(){
        boolean scale_unit = getScaleX()==1 && getScaleY()==1;
        boolean rotation_unit = getRotation()==0;
        setTransform(!scale_unit || !rotation_unit);
    }

    @Override
    protected void rotationChanged() {
        super.rotationChanged();
        checkTransform();
    }

    @Override
    public void setScaleX(float scaleX) {
        super.setScaleX(scaleX);
        checkTransform();
    }

    @Override
    public void setScaleY(float scaleY) {
        super.setScaleY(scaleY);
        checkTransform();
    }

    @Override
    public void setScale(float scaleXY) {
        super.setScale(scaleXY);
        checkTransform();
    }

    @Override
    public void setScale(float scaleX, float scaleY) {
        super.setScale(scaleX, scaleY);
        checkTransform();
    }

    @Override
    public void scaleBy(float scale) {
        super.scaleBy(scale);
        checkTransform();
    }

    @Override
    public void scaleBy(float scaleX, float scaleY) {
        super.scaleBy(scaleX, scaleY);
        checkTransform();
    }

    public boolean isPause() {
        return this.pause;
    }

    public void setPause(boolean var1) {
        this.pause = var1;
    }

    public Vector2 getCenterPosition() {
        return new Vector2(getX(Align.center),getY(Align.center));
    }

    @Override
    public String toString() {
        return "GGroup";
    }
}
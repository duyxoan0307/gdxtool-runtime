package editor.actor;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;

public class GTextField extends TextField {
    public GTextField(String text, Skin skin) {
        super(text, skin);
    }

    public GTextField(String text, TextFieldStyle textFieldStyle) {
        super(text, textFieldStyle);
    }
}

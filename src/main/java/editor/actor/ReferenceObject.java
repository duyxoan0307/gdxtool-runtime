package editor.actor;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import editor.object.GameObject;
import editor.object.component.Component;
import editor.util.A;

public class ReferenceObject<T> {
    public static Array<ReferenceObject> instances = new Array<>();
    private static boolean track = false;

    public transient T object;

    @A.ShowInInspector
    private int id=-1;
    private transient GameObject gameObject;
    private String className;
    public void setGameObject(GameObject gameObject){
        this.gameObject=gameObject;
    }
    public void setGameObjectAndCast(GameObject gameObject){
        this.setGameObject(gameObject);
        this.object=null;
        getObject();
    }
    public ReferenceObject(){
        if (track){
            instances.add(this);
        }
    }
    private T getObject(){
        if (object!=null){
            return object;
        }
        try {
            Class clazz=ClassReflection.forName(className);
            return (T)getObject(clazz);
        }catch (Throwable e){}
        return object;
    }
    public T getObject(Class<T> clazz){
        if (object!=null){
            return object;
        }
        object = castObject(clazz);
        return object;
    }
    public T castObject(Class<T> clazz){
        try {
            if (GameObject.class==clazz){
                return (T)gameObject;
            }
            if (ClassReflection.isAssignableFrom(Component.class,clazz)){
                Class<Component> componentClass = (Class<Component>) clazz;
                return (T) gameObject.getComponent(componentClass);
            }
            if (ClassReflection.isAssignableFrom(Actor.class,clazz)){
                if (ClassReflection.isAssignableFrom(clazz,gameObject.actor.getClass()))
                    return (T) gameObject.actor;
            }
        }catch (Throwable e){
        }
        return null;
    }
    public void setClassName(String s){
        this.className = s;
    }
    public int getId(){
        return id;
    }
    public void setId(int val){
        this.id=val;
    }
    public GameObject getGameObject(){
        return gameObject;
    }
    public static void startTrack(){
        instances.clear();
        track=true;
    }
    public static void stopTrack(){
        track=false;
    }
}

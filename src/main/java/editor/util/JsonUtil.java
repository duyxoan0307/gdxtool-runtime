package editor.util;

import com.badlogic.gdx.utils.*;
import editor.action.type.*;
import editor.actor.*;
import editor.object.GAlign;
import editor.object.GameObject;
import editor.object.cb.Run;
import editor.object.component.*;
import editor.object.data.*;
import editor.action.ActionData;
import editor.action.GInterpolation;

import java.util.HashMap;

public class JsonUtil {
    public static Json json = getJson(new MyJson());
    public static class MyJson extends Json{
        @Override
        public @Null Class getClass (String tag) {
            Class res = super.getClass(tag);
            if (res!=null){
                return res;
            }
            String name = getSimpleNameClass(tag);
            if (name.length()>0){
                return super.getClass(name);
            }
            return null;
        }

        @Override
        public Object readValue(Class type, Class elementType, JsonValue jsonData) {
            try {
                return super.readValue(type, elementType, jsonData);
            } catch (Throwable e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    public static String getSimpleNameClass(String tag){
        String name = tag.substring(tag.lastIndexOf('.') + 1);;
        name = name.substring(name.lastIndexOf('$') + 1);;
        return name;
    }
    public static JsonReader jsonReader = new JsonReader();
    public static Json getJson(Json res){
        json = res;
        res.setUsePrototypes(false);
        res.setIgnoreUnknownFields(true);
        res.setOutputType(JsonWriter.OutputType.json);
        res.setEnumNames(false);

        //core scene2d
        addClassTag(GGroup.class);
        addClassTag(GLabel.class);
        addClassTag(GParticle.class);
        addClassTag(GSpine.PooledSkeletonActor.class);
        addClassTag(GCustomDrawActor.class);

        //data
        addClassTag(ActorData.class);
        addClassTag(GameObjectData.class);
        addClassTag(GroupData.class);
        addClassTag(IData.class);
        addClassTag(ImageData.class);
        addClassTag(LabelData.class);
        addClassTag(ParticleData.class);
        addClassTag(ScrollData.class);
        addClassTag(SpineData.class);
        addClassTag(TableData.class);
        addClassTag(TransformData.class);

        //object
        addClassTag(GAlign.class);
        addClassTag(GameObject.class);

        //component
        addClassTag(ActionComponent.class);
        addClassTag(ButtonComponent.class);
        addClassTag(Component.class);
        addClassTag(PivotComponent.class);
        addClassTag(PrefabComponent.class);
        addClassTag(TableComponent.class);

        //action
        addClassTag(ActionData.class);
        addClassTag(GInterpolation.class);
        //action type
        addClassTag(GAddActionAction.class);
        addClassTag(GAddActionForAnother.class);
        addClassTag(GAlphaAction.class);
        addClassTag(GColorAction.class);
        addClassTag(GDelayAction.class);
        addClassTag(GDeletegateAction.class);
        addClassTag(GEventAction.class);
        addClassTag(GFadeInAction.class);
        addClassTag(GFadeOutAction.class);
        addClassTag(GForeverAction.class);
        addClassTag(GMoveByAction.class);
        addClassTag(GMoveToAction.class);
        addClassTag(GParallelAction.class);
        addClassTag(GRemoveAction.class);
        addClassTag(GRepeatAction.class);
        addClassTag(GRotateByAction.class);
        addClassTag(GRotateToAction.class);
        addClassTag(GRunnableAction.class);
        addClassTag(GScaleByAction.class);
        addClassTag(GScaleToAction.class);
        addClassTag(GSequenceAction.class);
        addClassTag(GSizeByAction.class);
        addClassTag(GSizeToAction.class);
        addClassTag(GTemporalAction.class);
        addClassTag(GTimeScaleAction.class);
        addClassTag(GTouchableAction.class);
        addClassTag(GVisibleAction.class);
        addClassTag(IAction.class);
        return res;
    }
    public static void addClassTag(Class clazz){
        json.addClassTag(clazz.getSimpleName(),clazz);
    }
    public static <T> String toJson(T t){
        return json.toJson(t);
    }
    public static <T> T fromJson(String data,Class<T> classOfT){
        return json.fromJson(classOfT,data);
    }
    public static<T> T cloneObject(T obj, T _default){
        try {
            T instance = _default;
            json.copyFields(obj,instance);
            return instance;
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }
    public static<T> T[] getArrayFromJson(String json, Class<T> clazz){
        JsonValue jsonValue = jsonReader.parse(json);
        Array<T> array = new Array<>();
        for (int i=0;i<jsonValue.size;i++){
            array.add(fromJson(jsonValue.get(i).toString(),clazz));
        }
        T[] res = array.toArray(clazz);
        return res;
    }
    public static<T,K> HashMap<T,K> getMapFromJson(String json, Class<T> clazzA, Class<K> clazzB){
        JsonValue jsonValue = jsonReader.parse(json);
        HashMap<T,K> map = new HashMap<T,K>();
        for (int i=0;i<jsonValue.size;i++){
            JsonValue child_value = jsonValue.get(i);
        }
        return map;
    }
    public static void traverse(String data, Run.ICallback<JsonValue> cb){
        traverseJsonValue(jsonReader.parse(data),cb);
    }
    public static void traverseJsonValue(JsonValue jsonValue, Run.ICallback<JsonValue> cb) {
        if (jsonValue.isObject()) {
            for (JsonValue child = jsonValue.child; child != null; child = child.next) {
                traverseJsonValue(child,cb);
            }
        } else if (jsonValue.isArray()) {
            for (JsonValue child : jsonValue) {
                traverseJsonValue(child,cb);
            }
        } else {
            if (jsonValue!=null){
                cb.run(jsonValue);
            }
        }
    }
}

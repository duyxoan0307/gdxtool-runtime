package editor.util;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import editor.provider.ToolProvider;

public class GUI {
    private static TextureRegion default_region = createRegion(20,20);

    public static TextureRegion getDefaultRegion(){
        return default_region;
    }
    static TextureRegion createRegion(int width, int height)
    {
        Pixmap pixmap = new Pixmap(width, height, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.WHITE);
        pixmap.fillRectangle(0, 0, width, height);
        Texture texture = new Texture(pixmap);
        pixmap.dispose();

        return new TextureRegion(texture);
    }
    public static TextField.TextFieldStyle getTextFieldStyle(String cursor_texture, String bg_texture, String selection_texture, String font_name) {
        Drawable cursor = new TextureRegionDrawable(ToolProvider.get().getTextureRegion(cursor_texture));
        Drawable selection = new TextureRegionDrawable(ToolProvider.get().getTextureRegion(bg_texture));
        Drawable background = new TextureRegionDrawable(ToolProvider.get().getTextureRegion(selection_texture));
        BitmapFont font = ToolProvider.get().getFont(font_name);
        TextField.TextFieldStyle textFieldStyle = new TextField.TextFieldStyle(font, Color.WHITE, cursor, selection, background);
        return textFieldStyle;
    }
    public static void fitLabel(Label text, float max_size){
        text.setFontScale(max_size);
        if (text.getWidth()<text.getPrefWidth())
        {
            text.setFontScale( text.getFontScaleX() * text.getWidth()/text.getPrefWidth());
        }
    }
    public static Color newColor(float r, float g, float b, float a){
        return new Color(r/255f,g/255f,b/255f,a);
    }
    public static Color newColor (String hex) {
        try{
            hex = hex.charAt(0) == '#' ? hex.substring(1) : hex;
            int r = Integer.valueOf(hex.substring(0, 2), 16);
            int g = Integer.valueOf(hex.substring(2, 4), 16);
            int b = Integer.valueOf(hex.substring(4, 6), 16);
            int a = hex.length() != 8 ? 255 : Integer.valueOf(hex.substring(6, 8), 16);
            return new Color(r / 255f, g / 255f, b / 255f, a / 255f);
        }catch (Throwable e){
            e.printStackTrace();
            return Color.WHITE;
        }
    }

    //set drawable
    public static void setRegion(Image img, TextureRegion textureRegion) {
        setRegion(img, textureRegion, false);
    }

    public static void setRegion(Image img, TextureRegion textureRegion, boolean is_resize) {
        setRegion(img, textureRegion, is_resize, 1);
    }

    public static void setRegion(Image img, TextureRegion textureRegion, boolean is_resize, int align) {
        setRegion(img,textureRegion,is_resize,false,align);
    }

    static Vector2 tmpVec = new Vector2();
    public static void setRegion(Image img, TextureRegion textureRegion, boolean is_resize, boolean is_resize_by_height, int align) {
        float cur_width = img.getWidth();
        float cur_height = img.getHeight();
        tmpVec.set(img.getX(align), img.getY(align));
        img.setDrawable(new TextureRegionDrawable(textureRegion));
        if (is_resize) {
            img.setSize((float)textureRegion.getRegionWidth(), (float)textureRegion.getRegionHeight());
        } else {
            if (is_resize_by_height){
                float tyle = cur_height * 1.0F / (float)textureRegion.getRegionHeight();
                img.setSize((float)textureRegion.getRegionWidth() * tyle,cur_height) ;
            }else{
                float tyle = cur_width * 1.0F / (float)textureRegion.getRegionWidth();
                img.setSize(cur_width, (float)textureRegion.getRegionHeight() * tyle);
            }
        }

        img.setPosition(tmpVec.x, tmpVec.y, align);
        img.setOrigin(1);
    }

    public static void setProgress(Image img, float percent, float original_width) {
        percent = MathUtils.clamp(percent, 0.0F, 1.0F);
        float cur_h = img.getHeight();Vector2 pos = tmpVec.set(img.getX(Align.left), img.getY(Align.center));
        TextureRegion curRegion = ((TextureRegionDrawable)img.getDrawable()).getRegion();
        Texture curTexture = curRegion.getTexture();
        TextureRegion newRegion = new TextureRegion(
                curTexture,curRegion.getRegionX(),curRegion.getRegionY(),(int)(original_width*percent),curRegion.getRegionHeight()
        );
        img.setDrawable(new TextureRegionDrawable(newRegion));
        img.setSize(original_width*percent, cur_h);
        img.setPosition(pos.x, pos.y, Align.left);
    }

    public static TextureRegion getTextureRegionProgress(TextureRegion textureRegion, float percent, float original_width) {
        percent = MathUtils.clamp(percent, 0.0F, 1.0F);
        float cur_h = textureRegion.getRegionHeight();
        TextureRegion curRegion = textureRegion;
        Texture curTexture = curRegion.getTexture();
        TextureRegion newRegion = new TextureRegion(
                curTexture,curRegion.getRegionX(),curRegion.getRegionY(),(int)(original_width*percent),curRegion.getRegionHeight()
        );
        return textureRegion;
    }


    public static void addActorNotChangePosition(Actor actor, Group parent){
        Vector2 stage_position = actor.localToStageCoordinates(tmpVec.set(actor.getWidth()/2,actor.getHeight()/2));
        addActor(parent,actor);
        Vector2 local_position=actor.getParent().stageToLocalCoordinates(stage_position);
        actor.setPosition(local_position.x,local_position.y,Align.center);
    }


    public static void addActor(Group parent, Actor actor){
        if (parent instanceof ScrollPane){
            ((ScrollPane) parent).setActor(actor);
        }else if (parent instanceof Table){
            ((Table) parent).add(actor);
        }else{
            parent.addActor(actor);
        }
    }

    public static void setProgressHeight(Image img, float percent, TextureRegion originRegion) {
        percent = MathUtils.clamp(percent, 0.0F, 1.0F);
        float cur_w = img.getWidth();
        Vector2 pos = tmpVec.set(img.getX(Align.center), img.getY(Align.bottom));
        TextureRegion curRegion = ((TextureRegionDrawable)img.getDrawable()).getRegion();
        Texture curTexture = curRegion.getTexture();
        int new_h = (int)(originRegion.getRegionHeight()*percent);
        TextureRegion newRegion = new TextureRegion(
                curTexture,curRegion.getRegionX(),(int)(originRegion.getRegionY()+(originRegion.getRegionHeight()-new_h)),curRegion.getRegionWidth(),new_h);
        img.setDrawable(new TextureRegionDrawable(newRegion));
        img.setSize(cur_w, new_h);
        img.setPosition(pos.x, pos.y, Align.bottom);
    }

    public static void setFilterLinear(Texture teture) {
        teture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
    }
    public static Vector2 actorStagePos(Actor actor){
        return new Vector2(actor.localToStageCoordinates(new Vector2().set(actor.getWidth()/2,actor.getHeight()/2)));
    }
}

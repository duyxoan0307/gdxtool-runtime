package editor.util;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;

public class AppType {
    public static boolean isDevMode = isDevMode();
    public static boolean isWebGL(){
        return Gdx.app.getType()== Application.ApplicationType.WebGL;
    }
    public static boolean isDesktop(){
        return Gdx.app.getType()==Application.ApplicationType.Desktop;
    }
    public static boolean isIOS(){
        return Gdx.app.getType()==Application.ApplicationType.iOS;
    }
    private static boolean isDevMode(){
        return (isDesktop() && !isJar());
    }
    public static boolean isJar(){
        return FileUtil.getFile("font").list().length==0;
    }
    public static boolean isAndroid(){
        return Gdx.app.getType()==Application.ApplicationType.Android;
    }
}

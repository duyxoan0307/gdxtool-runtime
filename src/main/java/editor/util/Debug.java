package editor.util;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;

public class Debug {
    public static void Log(Object log){
        Log("log",log);
    }
    public static void Log(String tag, Object log){
        String s_log = log==null?"null":log.toString();
        if (Gdx.app!=null)
            Gdx.app.log(tag,s_log);
        else {
            System.out.println(tag+": "+log);
        }
    }
    public static void Log(Object... objs){
       Log(getLogString(objs));
    }
    static String getLogString(Object... objs){
        StringBuilder stringBuilder = new StringBuilder();
        for (Object obj:objs){
            stringBuilder.append(obj.toString()+" | ");
        }
        return stringBuilder.toString();
    }
    public static void LogError(Object... objs){
        Log("\u001B[31m"+"ERROR: "+getLogString(objs)+"\u001B");
    }
}

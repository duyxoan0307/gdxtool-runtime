package editor.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

public class FileUtil {
    static String charset = "UTF-8";
    private static IFileProvider fileProvider = new IFileProvider() {
        @Override
        public FileHandle getFile(String path) {
            return Gdx.files.internal(path);
        }

        @Override
        public FileHandle getWriteFile(String path) {
            return Gdx.files.local(path);
        }

        @Override
        public FileHandle[] getListFile(FileHandle file) {
            return file.list();
        }
    };
    public static void setFileProvider(IFileProvider provider){
        fileProvider = provider;
    }
    public static FileHandle[] getListFile(FileHandle fileHandle){
        return fileProvider.getListFile(fileHandle);
    }
    public static FileHandle getFile(String path){
        return fileProvider.getFile(path);
    }
    public static void writeFile(String path,String s,boolean append){
        writeFile(fileProvider.getWriteFile(path),s,append);
    }

    public static void writeFile(FileHandle file, String s, boolean append){
        try{
            file.writeString(s,append,charset);
        }catch (Throwable e){
            e.printStackTrace();
        }
    }
    public static String readString(String path){
        return fileProvider.getFile(path).readString(charset);
    }
    public static String readString(FileHandle fileHandle){
        return fileHandle.readString(charset);
    }

    public interface IFileProvider{
        FileHandle getFile(String path);
        FileHandle getWriteFile(String path);
        FileHandle[] getListFile(FileHandle file);
    }
}

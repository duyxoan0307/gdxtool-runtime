package editor.provider;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.esotericsoftware.spine.AnimationStateData;
import com.esotericsoftware.spine.SkeletonData;
import editor.object.GameObject;
import editor.object.cb.Run;

public class ToolProvider {
    public static ToolProvider get(){
        return instance;
    }
    private static ToolProvider instance;
    public static void set(ToolProvider toolProvider){
        instance = toolProvider;
    }

    public int getW(){
        return 0;
    }
    public int getH(){
        return 0;
    }
    public TextureRegion getTextureRegion(String name){
        return null;
    }
    public BitmapFont getFont(String name){
        return null;
    }
    public void playSound(String name, float volume){
    }
    public Stage getStage(){return null;
    }
    public float halfStageW(){
        return 0;
    }
    public float halfStageH(){
        return 0;
    }
    public ParticleEffect getEffect(String name){
        return null;
    }
    public SkeletonData getSkeletonData(String name) {
        return null;
    }
    public AnimationStateData getAnimationStateData(String name) {
        return null;
    }
    public boolean isEditor(){
        return false;
    }
    public GameObject newGameObject(){
        return new GameObject();
    }
    public AssetManager getAssetManager(){
        return null;
    }
    public void notifyChange(String json, Run.ICallback<String> cb){}
    public String getFilePath(String fileName){
        return "";
    }
    public String getSpineExt(String spine){
        return ".skel";
    }
}

package editor.action;


import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.*;
import editor.action.type.*;
import editor.util.JsonUtil;

public class ActionParser {
    public static ActionData actionToData(Action action){
        ActionData actionData = new ActionData();
        actionData.action = getIAction(action);
        return actionData;
    }
    public static IAction getIAction(Action action){
        if (action==null){
            return null;
        }
        if (action instanceof DelayAction){
            return new GDelayAction().set((DelayAction) action);
        }
        if (action instanceof MoveToAction){
            return new GMoveToAction().set((MoveToAction)action);
        }
        if (action instanceof MoveByAction){
            return new GMoveByAction().set((MoveByAction) action);
        }
        if (action instanceof ScaleToAction){
            return new GScaleToAction().set((ScaleToAction)action);
        }
        if (action instanceof ScaleByAction){
            return new GScaleByAction().set((ScaleByAction) action);
        }
        if (action instanceof RotateToAction){
            return new GRotateToAction().set((RotateToAction) action);
        }
        if (action instanceof RotateByAction){
            return new GRotateByAction().set((RotateByAction) action);
        }
        if (action instanceof SequenceAction){
            return new GSequenceAction().set((SequenceAction)action);
        }
        if (action instanceof ParallelAction){
            return new GParallelAction().set((ParallelAction)action);
        }
        if (action instanceof RemoveAction){
            return new GRemoveAction();
        }
        if (action instanceof SizeByAction){
            return new GSizeByAction().set((SizeByAction) action);
        }
        if (action instanceof SizeToAction){
            return new GSizeToAction().set((SizeToAction) action);
        }
        if (action instanceof VisibleAction){
            return new GVisibleAction().set((VisibleAction) action);
        }
        if (action instanceof TouchableAction){
            return new GTouchableAction().set((TouchableAction)action);
        }
        if (action instanceof AddAction){
            return new GAddActionAction().set((AddAction)action);
        }
        if (action instanceof AlphaAction){
            return new GAlphaAction().set((AlphaAction)action);
        }
        return null;
    }
    public static Action dataToAction(ActionData actionData){
        return actionData.action.getAction();
    }
    public static String dataToJson(ActionData actionData){
        return JsonUtil.toJson(actionData);
    }
    public static Action jsonToAction(String json){
        return dataToAction(jsonToData(json));
    }
    public static ActionData jsonToData(String json){
        return (JsonUtil.fromJson(json,ActionData.class));
    }
    public static String actionToJson(Action action){
        return dataToJson(actionToData(action));
    }

    public static IAction cloneAction(IAction curAction) {
        return jsonToData(dataToJson(new ActionData().action(curAction))).action;
    }
}

package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class GRunnableAction implements IAction{
    public IAction action;
    @Override
    public Action getAction() {
        return Actions.run(()->{

        });
    }
}

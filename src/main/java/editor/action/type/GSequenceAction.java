package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;

public class GSequenceAction extends GParallelAction {
    @Override
    public Action getAction() {
        ParallelAction action = Actions.sequence();
        for (IAction child:actions){
            action.addAction(child.getAction());
        }
        return action;
    }
}

package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SizeToAction;

public class GSizeToAction extends GTemporalAction{
    public float width, height;
    public GSizeToAction set(SizeToAction action){
        width = action.getWidth();
        height = action.getHeight();
        return this;
    }
    @Override
    public Action getAction() {
        return Actions.scaleTo(width, height,duration,interpolation());
    }
}

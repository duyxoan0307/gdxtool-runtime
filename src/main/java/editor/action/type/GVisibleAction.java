package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.VisibleAction;

public class GVisibleAction implements IAction{
    public boolean visible=true;
    public GVisibleAction set(VisibleAction action){
        this.visible = action.isVisible();
        return this;
    }
    @Override
    public Action getAction() {
        return Actions.visible(visible);
    }
}

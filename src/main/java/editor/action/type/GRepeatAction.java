package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;

public class GRepeatAction extends GDeletegateAction{
    public int repeatCount;
    public GRepeatAction set(RepeatAction action){
        super.set(action);
        this.repeatCount = action.getCount();
        return this;
    }
    @Override
    public Action getAction() {
        return Actions.repeat(repeatCount,getIAction().getAction());
    }
}

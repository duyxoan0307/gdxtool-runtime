package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;

public class GAlphaAction extends GTemporalAction {
    public float a;
    public GAlphaAction set(AlphaAction action){
        super.set(action);
        a=action.getAlpha();
        return this;
    }

    @Override
    public Action getAction() {
        return Actions.alpha(a,duration,interpolation());
    }
}

package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;

public class GScaleToAction extends GTemporalAction{
    public float x,y;
    public GScaleToAction set(ScaleToAction action){
        super.set(action);
        x=action.getX();
        y=action.getY();
        return this;
    }
    @Override
    public Action getAction() {
        return Actions.scaleTo(x,y,duration,interpolation());
    }
}

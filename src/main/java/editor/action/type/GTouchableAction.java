package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.TouchableAction;

public class GTouchableAction implements IAction {
    public Touchable touchable = Touchable.enabled;
    public GTouchableAction set(TouchableAction action){
        touchable = action.getTouchable();
        return this;
    }
    @Override
    public Action getAction() {
        return Actions.touchable(touchable);
    }
}

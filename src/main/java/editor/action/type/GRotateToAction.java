package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;

public class GRotateToAction extends GTemporalAction {
    public float rotate;
    public GRotateToAction set(RotateToAction action){
        this.rotate = action.getRotation();
        return this;
    }

    @Override
    public Action getAction() {
        return Actions.rotateTo(rotate,duration,interpolation());
    }
}

package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

public class GEventAction implements IAction {
    public String event="";
    @Override
    public Action getAction() {
        return new Action() {
            @Override
            public boolean act(float delta) {
                StringEvent stringEvent = new StringEvent(event);
                getActor().fire(stringEvent);
                return true;
            }
        };
    }
    public static class StringEvent extends ChangeListener.ChangeEvent{
        public String event;
        public StringEvent(String s){
            this.event = s;
        }
    }
}

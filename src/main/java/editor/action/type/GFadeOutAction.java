package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class GFadeOutAction extends GAlphaAction {
    public GFadeOutAction(){
        a=0;
    }
    @Override
    public Action getAction() {
        return Actions.fadeOut(duration);
    }
}
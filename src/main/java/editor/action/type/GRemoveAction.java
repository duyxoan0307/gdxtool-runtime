package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class GRemoveAction implements IAction{
    @Override
    public Action getAction() {
        return Actions.removeActor();
    }
}

package editor.action.type;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class GColorAction extends GTemporalAction {
    public Color color = Color.WHITE;
    @Override
    public Action getAction() {
        return Actions.color(color,duration);
    }
}

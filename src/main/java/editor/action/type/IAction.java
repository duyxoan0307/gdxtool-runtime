package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;

public interface IAction {
    Action getAction();
}

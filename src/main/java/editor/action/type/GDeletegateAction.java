package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.DelegateAction;
import editor.action.ActionParser;

public abstract class GDeletegateAction implements IAction{
    private IAction action;
    public GDeletegateAction set(DelegateAction action){
        this.action= ActionParser.getIAction(action.getAction());
        return this;
    }
    @Override
    public Action getAction() {
        return null;
    }
    public IAction getIAction(){
        return action;
    }
    public void setIAction(IAction action){
        this.action = action;
    }
}

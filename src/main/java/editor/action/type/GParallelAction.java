package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;
import com.badlogic.gdx.utils.Array;
import editor.action.ActionParser;

public class GParallelAction implements IAction{
    public Array<IAction> actions = new Array<>();
    @Override
    public Action getAction() {
        ParallelAction action = Actions.parallel();
        for (IAction child:actions){
            action.addAction(child.getAction());
        }
        return action;
    }

    public GParallelAction set(ParallelAction action) {
        for (Action child:action.getActions()){
            actions.add(ActionParser.getIAction(child));
        }
        return this;
    }
}

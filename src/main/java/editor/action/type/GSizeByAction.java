package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SizeByAction;

public class GSizeByAction extends GTemporalAction{
    public float amountWidth, amountHeight;
    public GSizeByAction set(SizeByAction action){
        amountWidth = action.getAmountWidth();
        amountHeight = action.getAmountHeight();
        return this;
    }
    @Override
    public Action getAction() {
        return Actions.sizeBy(amountWidth, amountHeight,duration,interpolation());
    }
}

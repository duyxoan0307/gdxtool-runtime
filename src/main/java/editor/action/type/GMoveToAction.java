package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import editor.object.GAlign;

public class GMoveToAction extends GTemporalAction{
    public float x,y;
    public GAlign align = GAlign.CENTER;
    public GMoveToAction set(MoveToAction action){
        super.set(action);
        x=action.getX();
        y=action.getY();
        align=GAlign.getAlign(action.getAlignment());
        return this;
    }
    @Override
    public Action getAction() {
        return Actions.moveToAligned(x,y,align.align,duration,interpolation());
    }
}

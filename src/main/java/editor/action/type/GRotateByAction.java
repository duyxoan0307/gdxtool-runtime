package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RotateByAction;

public class GRotateByAction extends GTemporalAction {
    public float amount;
    public GRotateByAction set(RotateByAction action){
        this.amount = action.getAmount();
        return this;
    }

    @Override
    public Action getAction() {
        return Actions.rotateBy(amount,duration,interpolation());
    }
}

package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.TimeScaleAction;
import editor.action.ActionParser;

public class GTimeScaleAction extends GDeletegateAction{
    public float scale=1;
    public GTimeScaleAction set(TimeScaleAction timeScaleAction){
        this.scale = timeScaleAction.getScale();
        this.setIAction(ActionParser.getIAction(timeScaleAction.getAction()));
        return this;
    }
    @Override
    public Action getAction() {
        return Actions.timeScale(scale,getIAction().getAction());
    }

}

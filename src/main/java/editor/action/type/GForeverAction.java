package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class GForeverAction extends GDeletegateAction{
    @Override
    public Action getAction() {
        return Actions.forever(getIAction().getAction());
    }
}

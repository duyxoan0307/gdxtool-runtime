package editor.action.type;


import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.AddAction;
import editor.action.ActionParser;

public class GAddActionAction extends GDeletegateAction {
    public IAction action;
    public GAddActionAction set(AddAction action){
       this.action= ActionParser.getIAction(action);
        return this;
    }
    @Override
    public Action getAction() {
        return Actions.addAction(action.getAction());
    }
}

package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleByAction;

public class GScaleByAction extends GTemporalAction{
    public float x,y;
    public GScaleByAction set(ScaleByAction scaleByAction){
        this.x=scaleByAction.getAmountX();
        this.y=scaleByAction.getAmountY();
        return this;
    }
    @Override
    public Action getAction() {
        return Actions.scaleBy(x,y,duration,interpolation());
    }
}
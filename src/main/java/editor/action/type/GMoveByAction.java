package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;

public class GMoveByAction extends GTemporalAction{
    public float x,y;
    public GMoveByAction set(MoveByAction moveByAction){
        this.x=moveByAction.getAmountX();
        this.y=moveByAction.getAmountY();
        return this;
    }
    @Override
    public Action getAction() {
        return Actions.moveBy(x,y,duration,interpolation());
    }
}

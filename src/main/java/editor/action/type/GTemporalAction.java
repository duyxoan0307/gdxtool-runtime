package editor.action.type;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;
import editor.action.GInterpolation;

public abstract class GTemporalAction implements IAction {
    public float duration;
    public GInterpolation interpolation = GInterpolation.linear;
    public GTemporalAction set(TemporalAction action){
        duration = action.getDuration();
        interpolation = getInterpolation(action.getInterpolation());
        return this;
    }

    private GInterpolation getInterpolation(Interpolation interpolation) {
        for (GInterpolation i:GInterpolation.values()){
            if (i.interpolation==interpolation)
                return i;
        }
        return GInterpolation.linear;
    }


    public Interpolation interpolation() {
        if (interpolation==null){
            return Interpolation.linear;
        }
        return interpolation.interpolation;
    }
    @Override
    public Action getAction() {
        return null;
    }
}

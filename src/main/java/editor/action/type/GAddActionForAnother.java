package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

public class GAddActionForAnother extends GDeletegateAction{
    public String actorName="";
    public boolean clearAction;
    @Override
    public Action getAction() {
        return new Action() {
            @Override
            public void setActor(Actor actor) {
                super.setActor(actor);
                try{
                    if (actor!=null){
                        Group parent = actor.getParent();
                        if (parent==null && actor instanceof Group){
                            parent = (Group) actor;
                        }
                        Actor target = parent.findActor(actorName);
                        if (clearAction){
                            target.clearActions();
                        }
                        target.addAction(getIAction().getAction());
                    }
                }catch (Throwable e){
                    e.printStackTrace();
                }
            }
            @Override
            public boolean act(float delta) {
                return true;
            }
        };
    }
}

package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;

public class GDelayAction extends GDeletegateAction {
    public float delay;
    public GDelayAction set(DelayAction action){
        super.set(action);
        delay = action.getDuration();
        return this;
    }
    @Override
    public Action getAction() {
        IAction iAction = getIAction();
        if (iAction==null){
            return Actions.delay(delay);
        }else
            return Actions.delay(delay,iAction.getAction());
    }
}


package editor.action.type;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class GFadeInAction extends GAlphaAction {
    public GFadeInAction(){
        a=1;
    }
    @Override
    public Action getAction() {
        return Actions.fadeIn(duration);
    }
}

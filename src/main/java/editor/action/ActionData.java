package editor.action;

import editor.action.type.IAction;

public class ActionData {
    public IAction action;
    public String name="";
    public ActionData(){

    }
    public ActionData(IAction action){
        this.action = action;
    }
    public ActionData name(String s){
        this.name = s;
        return this;
    }
    public ActionData action(IAction action){
        this.action = action;
        return this;
    }
}

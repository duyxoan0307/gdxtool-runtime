package editor.action;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.Field;
import editor.util.Debug;

public enum GInterpolation {
    linear(Interpolation.linear),
    smooth(Interpolation.smooth),
    smooth2(Interpolation.smooth2),
    smoother(Interpolation.smoother),
    fade(Interpolation.fade),
    pow2(Interpolation.pow2),
    pow2In(Interpolation.pow2In),
    slowFast(Interpolation.slowFast),
    pow2Out(Interpolation.pow2Out),
    fastSlow(Interpolation.fastSlow),
    pow2InInverse(Interpolation.pow2InInverse),
    pow2OutInverse(Interpolation.pow2OutInverse),
    pow3(Interpolation.pow3),
    pow3In(Interpolation.pow3In),
    pow3Out(Interpolation.pow3Out),
    pow3InInverse(Interpolation.pow3InInverse),
    pow3OutInverse(Interpolation.pow3OutInverse),
    pow4(Interpolation.pow4),
    pow4In(Interpolation.pow4In),
    pow4Out(Interpolation.pow4Out),
    pow5(Interpolation.pow5),
    pow5In(Interpolation.pow5In),
    pow5Out(Interpolation.pow5Out),
    sine(Interpolation.sine),
    sineIn(Interpolation.sineIn),
    sineOut(Interpolation.sineOut),
    exp10(Interpolation.exp10),
    exp10In(Interpolation.exp10In),
    exp10Out(Interpolation.exp10Out),
    exp5(Interpolation.exp5),
    exp5In(Interpolation.exp5In),
    exp5Out(Interpolation.exp5Out),
    circle(Interpolation.circle),
    circleIn(Interpolation.circleIn),
    circleOut(Interpolation.circleOut),
    elastic(Interpolation.elastic),
    elasticIn(Interpolation.elasticIn),
    elasticOut(Interpolation.elasticOut),
    swing(Interpolation.swing),
    swingIn(Interpolation.swingIn),
    swingOut(Interpolation.swingOut),
    bounce(Interpolation.bounce),
    bounceIn(Interpolation.bounceIn),
    bounceOut(Interpolation.bounceOut);
    public Interpolation interpolation;
    GInterpolation(Interpolation interpolation){
        this.interpolation = interpolation;
    }

    //utils
    public static void setInterpolations(){
        Field[] fields = ClassReflection.getFields(Interpolation.class);
        String res = "";
        for (Field field:fields){
            String s = field.getName()+"(Interpolation."+field.getName()+"),\n";
            res+=s;
        }
    }
}

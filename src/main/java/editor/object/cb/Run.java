package editor.object.cb;

public class Run {
    public interface ICallback<T> {
        void run(T t);
    }
    public interface ICallback2<A,B> {
        void run(A A,B B);
    }
    public interface ICallback3<A,B,C> {
        void run(A A,B B, C C);
    }
    public interface ICallback4<A,B,C,D> {
        void run(A A,B B, C C, D D);
    }
    public interface ICallback5<A,B,C,D,E> {
        void run(A A,B B, C C, D D, E E);
    }
    public interface ICallbackValue<V,T> {
        V run(T t);
    }
}

package editor.object;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;

public enum GAlign {
    NONE(0),
    CENTER(Align.center),
    LEFT(Align.left),
    RIGHT(Align.right),
    TOP(Align.top),
    BOTTOM(Align.bottom),
    TOP_LEFT(Align.topLeft),
    TOP_RIGHT(Align.topRight),
    BOTTOM_LEFT(Align.bottomLeft),
    BOTTOM_RIGHT(Align.bottomRight),;
    public int align;
    GAlign(int align){
        this.align = align;
    }
    public static GAlign getAlign(int align){
        for (GAlign gAlign:values()){
            if (gAlign.align==align)
                return gAlign;
        }
        return null;
    }
    public static GAlign getAlign(int alignX, int alignY){
        if (alignX== CENTER.align && alignY== CENTER.align){
            return CENTER;
        }
        if (alignX== CENTER.align){
            return getAlign(alignY);
        }
        if (alignY==CENTER.align){
            return getAlign(alignX);
        }
        return getAlign(alignX|alignY);
    }
    public static GAlign getAlign(Actor actor){
        float originX = actor.getOriginX();
        float originY = actor.getOriginY();
        float w = actor.getWidth();
        float h = actor.getHeight();
        if (originX==0){
            if (originY==0){
                return BOTTOM_LEFT;
            }else if (originY==h){
                return TOP_LEFT;
            }else if (originY==h/2){
                return LEFT;
            }
        }else if (originX==w){
            if (originY==0){
                return BOTTOM_RIGHT;
            }else if (originY==h){
                return TOP_RIGHT;
            }else if (originY==h/2){
                return RIGHT;
            }
        }else if (originX == w/2){
            if (originY==0){
                return BOTTOM;
            }else if (originY==h){
                return TOP;
            }else if (originY==h/2){
                return CENTER;
            }
        }
        return NONE;
    }
}
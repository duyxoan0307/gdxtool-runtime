package editor.object.component;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import editor.object.GameObject;
import editor.sceneloader.Scene;

public class Component {
    public transient GameObject gameObject;
    public transient Actor actor;
    public boolean active = true;
    public void awake(){}
    public void start(){}
    public void update(float dt){
    }
    public void onDestroy(){}
    public void set(GameObject gameObject) {
        this.gameObject = gameObject;
        this.actor=gameObject.actor;
        awake();
    }
    public void setActive(boolean b){
        this.active = b;
    }
    public void onEditorDraw(Batch batch){};
    public void onEditorDrawSelecting(Batch batch){};
    public Actor getEditorActor(Runnable cb){
        return null;
    }

    public Actor cloneActor(Actor actor){
        return Scene.curScene.clone(actor);
    }

    public void addComponent(Component component){
        gameObject.addComponent(component);
    }
    public <T extends Component> void removeComponent(Class<T> tClass){
        gameObject.removeComponent(tClass);
    }
    public <T extends Component> T getComponent(Class<T> tClass){
        return gameObject.getComponent(tClass);
    }
}

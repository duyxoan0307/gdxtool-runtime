package editor.object.component;

import editor.object.GAlign;
import editor.actor.GActor;

public class PivotComponent extends Component{
    public GAlign align = GAlign.CENTER;
    @Override
    public void start() {
        super.start();

        GActor.get(gameObject.actor).pivot(align.align);
    }
}

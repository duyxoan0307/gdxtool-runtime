package editor.object.component;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;
import editor.actor.GActor;
import editor.object.GAlign;
import editor.object.GameObject;
import editor.provider.ToolProvider;

public class AnchorComponent extends Component{
    public GAlign align = GAlign.CENTER;
    public boolean stretchX,stretchY;
    public boolean scaleRatio = false;
    public enum Type{SCREEN,PARENT};
    public Type type=Type.SCREEN;

    @Override
    public void set(GameObject gameObject) {
        super.set(gameObject);

        if (!ToolProvider.get().isEditor()){
            if (type==Type.SCREEN){
                if (scaleRatio){
                    actor.setScale(getSceneScale());
                }else {
                    if (stretchX){
                        float x = actor.getX(align.align);
                        actor.setWidth(actor.getWidth()*getSceneScaleX());
                        actor.setX(x,align.align);
                    }
                    if (stretchY){
                        float y = actor.getY(align.align);
                        actor.setHeight(actor.getHeight()*getSceneScaleY());
                        actor.setY(y,align.align);
                    }
                }
                GActor.get(actor).pivot(align.align);
            }else if (type==Type.PARENT){
                if (actor.getParent()!=null){
                    Actor parent = actor.getParent();
                    GActor.get(actor).pivotParent(align.align);

                    if (stretchX){
                        actor.setWidth(parent.getWidth());
                        actor.setX(0,Align.left);
                    }
                    if (stretchY){
                        actor.setHeight(parent.getHeight());
                        actor.setY(0,Align.bottom);
                    }
                }
            }
        }
    }
    public static float getSceneScaleX(){
        return ToolProvider.get().getStage().getWidth()/ToolProvider.get().getW();
    }
    public static float getSceneScaleY(){
        return ToolProvider.get().getStage().getHeight()/ToolProvider.get().getH();
    }
    public static float getSceneScale(){
        float sclX = getSceneScaleX();
        float sclY = getSceneScaleY();
        return sclX<sclY?sclX:sclY;
    }
}

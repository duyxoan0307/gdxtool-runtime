package editor.object.component;

import com.badlogic.gdx.files.FileHandle;
import editor.action.ActionParser;
import editor.util.FileUtil;

public class ActionComponent extends Component{
    public String name;
    @Override
    public void start() {
        super.start();
        addAction();
    }

    void addAction(){
        FileHandle fileHandle = FileUtil.getFile("save/editor/action/"+name+".action");;
        if (fileHandle.exists()){
            gameObject.actor.addAction(ActionParser.jsonToAction(FileUtil.readString(fileHandle)));
        }
    }
}

package editor.object.component;

import editor.object.ActorUtils;
import editor.object.GameObject;

public class PrefabComponent extends Component{
    public String name="";

    @Override
    public void set(GameObject gameObject) {
        super.set(gameObject);

        if (name.equals("")){
            name= ActorUtils.getActorName(gameObject.actor);
        }
    }
}

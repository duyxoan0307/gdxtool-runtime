package editor.object.component;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import editor.actor.GActor;
import editor.actor.ReferenceObject;
import editor.object.GameObject;

public class ButtonComponent extends Component{
    public Array<ButtonData> datas = new Array<>();

    @Override
    public void start() {
        super.start();

        for (ButtonData buttonData:datas){
            Runnable cb = getMethod(buttonData);
            if (cb!=null){
                GActor.get(gameObject.actor).addListener(cb);
            }
        }
    }
    Runnable getMethod(ButtonData buttonData){
        try{
            MethodData methodData = buttonData.methodData;
            ReferenceObject ref = buttonData.ref;
            Class<Component> componentClazz = ClassReflection.forName(methodData.clazz);
            Component component = ref.getGameObject().getComponent(componentClazz);
            try{
                if (methodData.param!=null){
                    Class parseClazz = getPrimitiveClass(methodData.param.getClass());
                    return ()->{
                        try {
                            ClassReflection.getMethod(componentClazz,methodData.method,parseClazz).invoke(component,methodData.param);
                        }catch (Throwable e){
                            e.printStackTrace();
                        }
                    };
                }else {
                    return ()->{
                        try {
                            ClassReflection.getMethod(componentClazz,methodData.method).invoke(component);
                        }catch (Throwable e){
                            e.printStackTrace();
                        }
                    };
                }
            }catch (Throwable e){
                e.printStackTrace();
            }
        }catch (Throwable e){
        }
        return null;
    }
    static Class getPrimitiveClass(Class clazz){
        if (clazz==String.class){
            return String.class;
        }
        if (clazz==Integer.class){
            return int.class;
        }
        if (clazz==Float.class){
            return float.class;
        }
        if (clazz==Boolean.class){
            return boolean.class;
        }
        if (clazz==Long.class){
            return long.class;
        }
        if (clazz==Double.class){
            return double.class;
        }
        return clazz;
    }

    public static class ButtonData{
        public ReferenceObject<GameObject> ref = new ReferenceObject();
        public MethodData methodData = new MethodData();
    }
    public static class MethodData{
        public String clazz;
        public String method;
        public Object param;
        public transient ButtonData buttonData;
    }
}

package editor.object.component;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;

public class TableComponent extends Component{
    public int row=1,pad;

    @Override
    public void start() {
        Table table = (Table) gameObject.actor;
        Array<Actor> childs = new Array<>(table.getChildren());
        table.clearChildren();
        for (Actor child:childs){
            table.add(child).pad(pad);
            if (table.getChildren().size%row==0)
                table.row();
        }
    }
}

package editor.object;

import editor.actor.GGroup;

public class GroupObject extends GGroup {
    @Override
    public void act(float delta) {
        updateGameObjects(delta);
        super.act(delta);
    }

    public void updateGameObjects(float dt) {
        ActorUtils.traverseActor(this, actor -> {
            GameObjectUtils.getGameObjectFromActor(actor, go -> go.update(dt));
        }, actor -> actor.isVisible());
    }
}

package editor.object;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.ObjectSet;
import editor.object.cb.Run;
import editor.actor.GActor;
import editor.provider.ToolProvider;
import editor.util.FileUtil;
import editor.util.GUI;

public class ActorUtils {
    public static String getTextureName(Image img){
        TextureRegionDrawable textureRegionDrawable = (TextureRegionDrawable) img.getDrawable();
        TextureRegion textureRegion = textureRegionDrawable.getRegion();
        if (textureRegion instanceof TextureAtlas.AtlasRegion){
            return ((TextureAtlas.AtlasRegion)textureRegion).name;
        }
        if (textureRegion.getTexture()== GUI.getDefaultRegion().getTexture()){
            Object textureFromUserData = GActor.getUserObject(img,GameObjectUtils.TEXTURE_KEY);
            if (textureFromUserData!=null && textureFromUserData instanceof String){
                return (String) textureFromUserData;
            }else {
                return "NULL";
            }
        }
        String texturePath = textureRegionDrawable.getRegion().getTexture().getTextureData().toString();
        String name = FileUtil.getFile(texturePath).nameWithoutExtension();
        return name;
    }
    public static ObjectSet<TextureAtlas> atlasSet;

    public static String getAtlasName(Image img) {
        TextureRegionDrawable textureRegionDrawable = (TextureRegionDrawable) img.getDrawable();
        TextureRegion region = textureRegionDrawable.getRegion();
        if (!(region instanceof TextureAtlas.AtlasRegion)){
            return "";
        }
        Texture texture = region.getTexture();;
        for (TextureAtlas atlas:atlasSet){
            if (atlas.getTextures().contains(texture)){
                FileHandle fileHandle = new FileHandle(ToolProvider.get().getAssetManager().getAssetFileName(atlas));
                String name = fileHandle.nameWithoutExtension();
                return name;
            }
        }
        return "";
    }
    public static String getActorName(Actor actor){
        if (actor.getName()==null){
            return actor.getClass().getSimpleName();
        }
        return actor.getName();
    }

    public static void traverseActor(Actor actor, Run.ICallback<Actor> cb){
        cb.run(actor);
        if (actor instanceof Group){
            for (Actor child:((Group) actor).getChildren()){
                traverseActor(child,cb);
            }
        }
    }

    public static void traverseActor(Actor actor, Run.ICallback<Actor> cb, Run.ICallbackValue<Boolean,Actor> callBack){
        if (callBack.run(actor)){
            cb.run(actor);
            if (actor instanceof Group){
                for (Actor child:((Group) actor).getChildren()){
                    traverseActor(child,cb,callBack);
                }
            }
        }
    }

}

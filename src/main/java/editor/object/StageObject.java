package editor.object;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;
import editor.object.cb.Run;

public class StageObject extends Stage {
    public StageObject(Viewport viewport, Batch batch) {
        super(viewport,batch);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        updateGameObjects(delta);
    }

    public void updateGameObjects(float dt) {
        ActorUtils.traverseActor(getRoot(), actor -> {
            GameObjectUtils.getGameObjectFromActor(actor, go -> go.update(dt));
        }, actor -> actor.isVisible());
    }
}

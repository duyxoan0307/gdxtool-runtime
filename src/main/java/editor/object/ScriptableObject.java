package editor.object;

import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import editor.util.JsonUtil;

public class ScriptableObject {
    private String type="";
    public String name="";
    public static <T extends ScriptableObject>T fromJson(String s){
        JsonValue jsonValue = JsonUtil.jsonReader.parse(s);
        String type = jsonValue.get("type").asString();
        try{
            Class clazz = JsonUtil.json.getClass(type);
            return (T)JsonUtil.fromJson(s,clazz);
        }catch (Throwable e){}
        return null;
    }
    public String toJson(){
        this.type=getClass().getName();
        return JsonUtil.toJson(this);
    }
}

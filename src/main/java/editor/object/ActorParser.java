package editor.object;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.esotericsoftware.spine.utils.SkeletonActor;
import editor.actor.ReferenceObject;
import editor.object.data.*;
import editor.actor.GParticle;
import editor.util.JsonUtil;

public class ActorParser {
    public static String actorToJson(Actor actor){
        return JsonUtil.toJson(actorToData(actor));
    }
    public static Actor jsonToActor(String json){
        ActorData actorData = JsonUtil.fromJson(json,ActorData.class);
        Actor actor = dataToActor(actorData);
        return actor;
    }
    public static ActorData actorToData(Actor actor){
        ActorData actorData = new ActorData().setActor(actor);
        actorData.addData(new TransformData().set(actor));
        if (actor instanceof Image){
            actorData.addData(new ImageData().set((Image) actor));
        }else if (actor instanceof Label){
            actorData.addData(new LabelData().set(actor));
        }else if (actor instanceof SkeletonActor){
            actorData.addData(new SpineData().set((SkeletonActor) actor));
        }else if (actor instanceof GParticle){
            actorData.addData(new ParticleData().set((GParticle) actor));
        }else if (actor instanceof Group){
            if (actor instanceof ScrollPane){
                actorData.addData(new ScrollData().set((ScrollPane) actor));
            }else if (actor instanceof Table){
                actorData.addData(new TableData().set((Table) actor));
            }else{
                actorData.addData(new GroupData().set((Group) actor));
            }
        }
        GameObject gameObject = GameObjectUtils.getGameObjectFromActor(actor);
        if (gameObject!=null){
            actorData.addData(new GameObjectData().setActor(actor));
        }
        return actorData;
    }
    public static Actor dataToActor(ActorData actorData){
        String type = actorData.actorType;
        Actor actor = getNewActorInstance(type);
        for (Object data:actorData.dataArray){
            if (data instanceof IData){
                ((IData)data).apply(actor);
            }
        }
        return actor;
    }
    public static Actor getNewActorInstance(String type){
        if (type.equals(ScrollPane.class.getName())){
            return new ScrollPane(null);
        }
        try{
            Class clazz = JsonUtil.json.getClass(type);
            if (clazz==null){
                clazz=getClazzFromName(type);
            }
            Actor actor = (Actor) ClassReflection.newInstance(clazz);
            return actor;
        }catch (Throwable e){
            e.printStackTrace();
        }
        return new Actor();
    }
    public static Class getClazzFromName(String name){
        try {
            Class clazz = ClassReflection.forName(name);
            return clazz;
        }catch (Throwable e){
            return null;
        }
    }

    public static <T extends Actor> T cloneActor(Actor copyActor) {
        ReferenceObject.startTrack();
        Actor actor = (T)jsonToActor(actorToJson(copyActor));
        ReferenceObject.stopTrack();
        return (T)actor;
    }
}

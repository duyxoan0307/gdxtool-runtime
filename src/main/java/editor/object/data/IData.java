package editor.object.data;

import com.badlogic.gdx.scenes.scene2d.Actor;

public interface IData {
    void apply(Actor actor);
}

package editor.object.data;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import editor.object.ActorParser;

public class ScrollData implements IData{
    public ActorData childData;
    public boolean disableScrollX, disableScrollY;
    public ScrollData set(ScrollPane scrollPane){
        Actor childActor = scrollPane.getActor();
        if (childActor!=null)
            this.childData = ActorParser.actorToData(childActor);
        this.disableScrollX = scrollPane.isScrollingDisabledX();
        this.disableScrollY = scrollPane.isScrollingDisabledY();
        return this;
    }
    @Override
    public void apply(Actor actor) {
        ScrollPane scrollPane = (ScrollPane) actor;
        scrollPane.setScrollingDisabled(disableScrollX,disableScrollY);
        if (childData!=null){
            Actor child = ActorParser.dataToActor(childData);
            scrollPane.setActor(child);
        }
    }
}

package editor.object.data;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;
import editor.actor.GActor;
import editor.object.ActorUtils;

public class TransformData implements IData{
    public String name;
    public float x,y,w,h,sclX,sclY,orgX,orgY,rotate;
    public Color color;
    public boolean isVisible = true;
    public boolean debug;
    public TransformData set(Actor actor){
        this.x=actor.getX();
        this.y=actor.getY();
        this.w=actor.getWidth();
        this.h=actor.getHeight();
        this.rotate=actor.getRotation();
        this.sclX=actor.getScaleX();
        this.sclY=actor.getScaleY();
        this.orgX=actor.getOriginX();
        this.orgY=actor.getOriginY();
        this.color = actor.getColor();
        this.name = ActorUtils.getActorName(actor);
        this.isVisible = actor.isVisible();
        this.debug = actor.getDebug();
        return this;
    }
    @Override
    public void apply(Actor actor) {
        GActor.get(actor).name(name).size(w,h).pos(x,y, Align.bottomLeft).rotate(rotate).scl(sclX,sclY)
                .origin(orgX,orgY).color(color).visible(isVisible).debug(debug);
    }
}

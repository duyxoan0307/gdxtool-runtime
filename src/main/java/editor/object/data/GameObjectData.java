package editor.object.data;

import com.badlogic.gdx.scenes.scene2d.Actor;
import editor.object.GameObject;
import editor.object.GameObjectUtils;
import editor.object.component.Component;

public class GameObjectData implements IData{
    private GameObject gameObject;
    public GameObjectData setActor(Actor actor){
        this.gameObject = GameObjectUtils.getGameObjectFromActor(actor);
        return this;
    }
    public GameObject getGameObject(){
        return gameObject;
    }
    @Override
    public void apply(Actor actor) {
        if (gameObject!=null)
        {
            GameObjectUtils.setGameObject(actor,gameObject);
            for (int i=gameObject.components.size-1;i>=0;i--){
                if (gameObject.components.get(i)==null)
                    gameObject.components.removeIndex(i);
            }
            for (Component component:gameObject.components){
                if (component!=null)
                    component.set(gameObject);
            }
        }
    }
}

package editor.object.data;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import editor.object.GAlign;
import editor.provider.ToolProvider;
import editor.actor.GActor;
import editor.actor.GLabel;

public class LabelData implements IData{
    public String font;
    public String text;
    public float fontScale;
    public boolean wrap;
    public GAlign align=GAlign.CENTER;
    public float fitMaxScl=1;
    public boolean fit;
    public LabelData set(Actor actor){
        Label label = (Label) actor;
        font = label.getStyle().font.getData().name;
        text = label.getText().toString();
        fontScale = label.getFontScaleX();
        wrap = label.getWrap();
        align = GAlign.getAlign(label.getLabelAlign());

        Float fit = (Float) GActor.getUserObject(actor, GLabel.FIT_KEY);
        if (fit!=null){
            this.fit=true;
            this.fitMaxScl=fit;
        }

        return this;
    }
    @Override
    public void apply(Actor actor) {
        Label lbl = GActor.get(actor).font(ToolProvider.get().getFont(font)).text(text).wrap(wrap).fontScl(fontScale).alignLabel(align.align).get();
        if (fit){
            GActor.get(lbl).fitLabel(fitMaxScl);
        }else {
            GActor.removeUserObject(actor,GLabel.FIT_KEY);
        }
    }
}

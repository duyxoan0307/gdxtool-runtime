package editor.object.data;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonData;
import com.esotericsoftware.spine.utils.SkeletonActor;
import editor.provider.ToolProvider;
import editor.actor.GSpine;

public class SpineData implements IData{
    public String spine;
    public String anim;
    public boolean loop;
    public String skin;
    public float scl = 1;
    public SpineData set(SkeletonActor actor){
        this.spine = actor.getSkeleton().getData().getName();
        this.spine+=getExt(this.spine);
        AnimationState.TrackEntry trackEntry = actor.getAnimationState().getTracks().first();
        anim = trackEntry.getAnimation().getName();
        skin = actor.getSkeleton().getSkin().getName();
        loop = trackEntry.getLoop();
        scl = actor.getSkeleton().getScaleX();
        return this;
    }

    private String getExt(String spine) {
        return ToolProvider.get().getSpineExt(spine);
    }

    @Override
    public void apply(Actor actor) {
        SkeletonActor skeletonActor = (SkeletonActor) actor;
        skeletonActor.setRenderer(GSpine.skeletonRenderer);
        SkeletonData skeletonData = ToolProvider.get().getSkeletonData(spine);
        skeletonActor.setSkeleton(new Skeleton(skeletonData));
        skeletonActor.setAnimationState(new AnimationState(ToolProvider.get().getAnimationStateData(spine)));

        skeletonActor.getAnimationState().setAnimation(0,skeletonActor.getAnimationState().getData().getSkeletonData().getAnimations().first(),true);
        skeletonActor.getSkeleton().setSkin(skeletonData.getSkins().peek().getName());

        skeletonActor.getAnimationState().setAnimation(0,anim,loop);
        skeletonActor.getSkeleton().setSkin(skin);
        skeletonActor.getSkeleton().setScale(scl,scl);
    }
}

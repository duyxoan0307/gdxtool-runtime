package editor.object.data;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import editor.actor.GActor;
import editor.object.ActorUtils;
import editor.provider.ToolProvider;

public class ImageData implements IData{
    public String texture="";
    private String atlas="";
    public ImageData(){
    }
    public ImageData set(Image image){
        this.texture = ActorUtils.getTextureName(image);
        if (ToolProvider.get().isEditor())
            this.atlas = ActorUtils.getAtlasName(image);
        return this;
    }
    @Override
    public void apply(Actor actor) {
        GActor.get(actor).drawable(texture);
    }
    public String getAtlas(){
        return atlas;
    }
}

package editor.object.data;

import com.badlogic.gdx.scenes.scene2d.Actor;
import editor.actor.GParticle;

public class ParticleData implements IData{
    public String particle;
    public boolean loop;
    public ParticleData set(GParticle actor){
        particle = actor.particleName;
        this.loop = actor.loop;
        return this;
    }
    @Override
    public void apply(Actor actor) {
        GParticle particle = (GParticle) actor;
        particle.change(this.particle).loop(loop);
    }
}

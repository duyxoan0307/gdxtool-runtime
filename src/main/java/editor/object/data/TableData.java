package editor.object.data;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import editor.object.ActorParser;

public class TableData implements IData{
    public Array<ActorData> childs = new Array<>();
    public TableData set(Table table){
        for (Actor child:table.getChildren()){
            childs.add(ActorParser.actorToData(child));
        }
        return this;
    }
    @Override
    public void apply(Actor actor) {
        Table table = (Table) actor;
        table.clearChildren();
        for (ActorData childData:childs){
            Actor child = ActorParser.dataToActor(childData);
            table.add(child);
        }
    }
}

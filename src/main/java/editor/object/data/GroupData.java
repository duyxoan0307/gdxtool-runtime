package editor.object.data;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import editor.object.ActorParser;


public class GroupData implements IData{
    private Array<ActorData> childs = new Array<>();
    public GroupData set(Group group){
        for (Actor child:group.getChildren()){
            childs.add(ActorParser.actorToData(child));
        }
        return this;
    }
    @Override
    public void apply(Actor actor) {
        Group group = (Group) actor;
        for (ActorData childData:childs){
            group.addActor(ActorParser.dataToActor(childData));
        }
    }
}

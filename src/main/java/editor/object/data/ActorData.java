package editor.object.data;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import editor.util.JsonUtil;

public class ActorData {
    public String actorType;
    public Array<Object> dataArray = new Array();

    public ActorData setActor(Actor actor) {
        String clazzName = JsonUtil.json.getTag(actor.getClass());
        if (clazzName==null){
            clazzName=actor.getClass().getName();;
        }
        this.actorType=clazzName;
        return this;
    }
    public ActorData addData(IData data){
        dataArray.add(data);
        return this;
    }
    public Object getData(Class clazz){
        for (Object obj:dataArray){
            if (obj.getClass()==clazz){
                return obj;
            }
        }
        return null;
    }
}

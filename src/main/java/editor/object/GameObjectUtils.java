package editor.object;

import com.badlogic.gdx.scenes.scene2d.Actor;
import editor.actor.GActor;
import editor.object.cb.Run;
import editor.provider.ToolProvider;

public class GameObjectUtils {
    public static final String GAME_OBJECT_KEY = "gameObject";
    public static final String TEXTURE_KEY = "TEXTURE";
    public static GameObject getGameObjectFromActor(Actor actor){
        if (actor==null)
            return null;
        Object obj = GActor.getUserObject(actor,GAME_OBJECT_KEY);
        if (obj!=null && obj instanceof GameObject){
            return (GameObject) obj;
        }
        return null;
    }
    public static GameObject getGameObjectFromActor(Actor actor, Run.ICallback<GameObject> cb){
        GameObject gameObject = getGameObjectFromActor(actor);
        if (gameObject!=null){
            cb.run(gameObject);
        }
        return gameObject;
    }
    public static GameObject getOrAddGameObject(Actor actor){
        GameObject gameObject = getGameObjectFromActor(actor);
        if (gameObject==null){
            return addGameObject(actor);
        }
        return gameObject;
    }
    public static GameObject addGameObject(Actor actor){
        GameObject gameObject = ToolProvider.get().newGameObject();
        setGameObject(actor,gameObject);
        return gameObject;
    }
    public static void setGameObject(Actor actor, GameObject gameObject) {
        GActor.setUserObject(actor,GAME_OBJECT_KEY,gameObject);
        gameObject.setActor(actor);
    }
    public static void removeGameObject(Actor actor){
        GActor.setUserObject(actor,GAME_OBJECT_KEY,null);
    }

}

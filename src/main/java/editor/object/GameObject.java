package editor.object;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import editor.object.component.Component;


public class GameObject {
    public int uuid;
    public transient Actor actor;
    public Array<Component> components =new Array<>();
    public GameObject setActor(Actor actor){
        this.actor = actor;
        return this;
    }
    public void addComponent(Component component){
//        if (getComponent(component.getClass())!=null)
//            return;
        components.add(component);
        component.set(this);
    }
    public <T extends Component> void removeComponent(Class<T> tClass){
        for (int i=0;i<components.size;i++){
            Component component = components.get(i);
            if (component.getClass()==tClass){
                components.removeIndex(i);
                return;
            }
        }
    }
    public <T extends Component> T getComponent(Class<T> tClass){
        for (int i=0;i<components.size;i++){
            Component component = components.get(i);
            if (ClassReflection.isAssignableFrom(tClass,component.getClass())){
                return (T)component;
            }
        }
        return null;
    }
    public void update(float dt){
        for (Component component:components){
            if (component.active)
                component.update(dt);
        }
    }

    public void start(){
        for (Component component:components){
            if (component.active)
                component.start();
        }
    }
    public void destroy(){
        ActorUtils.traverseActor(actor, actor -> {
            GameObjectUtils.getGameObjectFromActor(actor, gameObject -> gameObject.destroyComponents());
        });
        actor.remove();
    }
    void destroyComponents(){
        for (Component component:components){
            component.onDestroy();
        }
        components.clear();
    }
}
